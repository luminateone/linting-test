#!/bin/bash

# Create a config file if it does not exist
create_config_file(){
    FILE="$1";
    echo "config file => $1"
    if [ ! -f $FILE ]; then
        echo "$2" > $1
    fi
}

php_stan(){
    echo ""
    echo "Checking PHP - phpstan / larastan"
    echo "================================="

    # install package if not already installed
    grep larastan "composer.json" > /dev/null || composer require --dev nunomaduro/larastan

    # create the config file
    FILE=".phpstan.neon"
    create_config_file "$FILE" "includes:
    - ./vendor/nunomaduro/larastan/extension.neon

parameters:

    paths:
        - app

    # The level 8 is the highest level
    # https://phpstan.org/user-guide/rule-levels
    level: 6

    excludes_analyse:
        - */Nova/*

    checkMissingIterableValueType: false
"
    # run phpstan
    ./vendor/bin/phpstan analyse -c $FILE --memory-limit=8G --xdebug
}

php_cs_fixer(){
    echo ""
    echo "Fixing PHP - php-cs-fixer"
    echo "================================="

    # create the config file
    create_config_file '.php_cs' "<?php

\$finder = Symfony\Component\Finder\Finder::create()
    ->in(__DIR__ . '/app')
    ->in(__DIR__ . '/config')
    ->in(__DIR__ . '/database')
    ->in(__DIR__ . '/routes')
    ->in(__DIR__ . '/tests')
    ->name('*.php')
    ->notName('*.blade.php');

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2' => true,
        'array_syntax' => ['syntax' => 'short'],
        'ordered_imports' => ['sortAlgorithm' => 'alpha'],
        'no_unused_imports' => true,
    ])
    ->setFinder(\$finder);

?>"

    # install package if not already installed
    grep php-cs-fixer "composer.json" > /dev/null || composer require --dev friendsofphp/php-cs-fixer --ignore-platform-reqs

    # run php-cs-fixer
    ./vendor/bin/php-cs-fixer fix

	# check cache is ignored by git
	grep php_cs.cache .gitignore > /dev/null || echo .php_cs.cache >> .gitignore
}


eslint_packages=("eslint@latest" "prettier" "eslint-loader" "eslint-plugin-vue" "eslint-plugin-standard" "eslint-plugin-promise" "eslint-plugin-import" "eslint-plugin-node" "eslint-config-prettier" "eslint-config-standard" "eslint-plugin-prettier")

# remove and readd all eslint packages
eslint_fix(){
    npm remove "$eslint_packages"
    npm install "$eslint_packages"
}

# install and run jslint
js_eslint(){
    echo ""
    echo "Checking Javascript - eslint"
    echo "================================="

    # check packages are installed'
    for package in "${eslint_packages[@]}"
    do
        grep $package "package.json" > /dev/null || npm install $package --save-dev
    done

    create_config_file ".eslintrc.json" '{
    "env": {
        "browser": true,
        "es2020": true,
        "amd": true,
        "node": true
    },
    "extends": [
        "standard",
        "plugin:vue/recommended",
        "plugin:vue/essential",
        "prettier",
        "prettier/standard",
        "prettier/vue"
    ],
    "parserOptions": {
        "ecmaVersion": 11,
        "sourceType": "module"
    },
    "plugins": [
        "vue",
		"prettier"
    ],
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single",
            { "avoidEscape": true }
        ],
        "semi": [
            "error",
            "always"
        ],
        "eqeqeq": ["error", "always"]
    }
}'

    create_config_file ".prettierrc.json" '{
    "trailingComma": "es5",
    "tabWidth": 4,
    "semi": true,
    "singleQuote": true
}'

	./node_modules/.bin/eslint "resources/**/*.{js,vue}" --fix

	# check frontend only if it exists
	if [ -d frontend ]; then
	    ./node_modules/.bin/eslint "frontend/**/*.{js,vue}" --fix
	fi;
}

git_push_hook(){
    echo ""
    echo "Checking git push hook"
    echo "================================="

    CURRENT_PATH=`dirname $0`

    # create the config file
    create_config_file '.huskyrc.json' "{
    \"hooks\": {
        \"pre-push\": \"bash ${CURRENT_PATH//\\//}/lint.sh\"
    }
}"

	  # remove hook files if husky file not present
	  if [ ! -f .git/hooks/husky.sh ]; then
        rm .git/hooks/pre-push
    fi

    # check packages are installed
    grep husky "package.json" > /dev/null || npm install husky --save-dev
}

php_storm(){

	if [ ! -d .idea ]; then
		mkdir -p .idea;
	fi;

	if [ ! -d .idea/jsLinters ]; then
		mkdir -p .idea/jsLinters;
	fi;

    # Enable eslint in php storm
    create_config_file ".idea/jsLinters/eslint.xml" '<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="EslintConfiguration">
    <option name="fix-on-save" value="true" />
  </component>
</project>'
}

check_php(){
	if ! php --version;  then
		echo
		echo 'PHP was not found you need to add php to your $PATH'
		echo 'see https://www.php.net/manual/en/faq.installation.php#faq.installation.addtopath'
		return 1
	fi
}

ide_helper(){
    echo ""
    echo "Updating IDE Helper comments"
    echo "================================="

	grep laravel-ide-helper "composer.json" > /dev/null || composer require --dev barryvdh/laravel-ide-helper --ignore-platform-reqs

	php artisan ide-helper:models --dir="app/Models" -W
	php artisan ide-helper:generate
	php artisan ide-helper:meta
}

check_php \
	&& git_push_hook \
	&& ide_helper \
	&& php_cs_fixer \
	&& php_storm \
	&& php_stan \
	&& js_eslint

EXIT_CODE=$?

# line breaks before git console messages
echo
echo
exit $EXIT_CODE
